<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String </h1>

    <?php   
    echo "<h3> Soal No 1</h3>";

    $kalimat1 = "selamat datang di sanbarcode";

    echo "panjang string kalimat 1: " . strlen($kalimat1) . "<br>";
    echo "panjang kata kalimat 1: " . str_word_count($kalimat1) . "<br>";

        $first_sentence = "Hello PHP!" ; 
        $second_sentence = "I'm ready for the challenges"; 
        
        echo "<h3> Soal No 2</h3>";

        $kalimat2 = "lorem ipsum dolar amet";
        
        echo "Kata pertama kalimat 2: " . substr($kalimat2, 0, 5) . "<br>" ; 
        echo "Kata kedua kalimat 2: " . substr($kalimat2, 6, 5) . "<br>" ; 
        echo "Kata ketiga kalimat 2: " . substr($kalimat2, 12, 5) . "<br>" ; 
        echo "Kata keempat kalimat 2: " . substr($kalimat2, 18) . "<br>" ; 

        echo "<h3> Soal No 3 </h3>";

        $kalimat3 = "Saya sedang belajar";
        
        echo "Sebelum: " . $kalimat3 . "<br>"; 
        echo "Sesudah: " . str_replace("belajar", "makan", $kalimat3) . "<br>";
    ?>
</body>
</html>